﻿using System;  
using System.Speech.Synthesis;  
using System.Speech.AudioFormat;  

namespace SampleSynthesis  
{  
  class Program  
  {  
    static void Main(string[] args)  
    {  
      SpeechSynthesizer speech = new SpeechSynthesizer();
      
        
        var voiceLanguage ="";
        foreach (InstalledVoice voice in speech.GetInstalledVoices())  
        {  
         if (voice.VoiceInfo.Name.Equals("Microsoft Helia")){
           voiceLanguage=voice.VoiceInfo.Name;
         } 
    }  
    speech.SelectVoice(voiceLanguage);
        speech.Rate=-5;
        speech.Speak("O Álvaro é um gato!");
      
  }  
}
}
 